       IDENTIFICATION DIVISION.
       PROGRAM-ID. LISTING5-2.
       AUTHOR. NitroEz.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 CITYCODE             PIC 9 VALUE ZERO.
          88 CITYISDUBLIN            VALUE 1.
          88 CITYISLIMERICK          VALUE 2.
          88 CITYISCORK              VALUE 3.
          88 CITYISGALWAY            VALUE 4.
          88 CITYISSLIGO             VALUE 5.
          88 CITYISWATERFORD         VALUE 6.
          88 UNIVERSITYCITY          VALUE 1 THRU 4.
          88 CITYCODENOTVALID        VALUE 0
                                       , 7, 8, 9.

       PROCEDURE DIVISION.
       BEGIN.
           DISPLAY "Enter a city code (1-6) - " WITH NO ADVANCING
           ACCEPT CITYCODE
           IF CITYCODENOTVALID
              DISPLAY "Invalid city code entered"
           ELSE
              IF CITYISLIMERICK
                 DISPLAY "Hey, we're home."
              END-IF
              IF CITYISDUBLIN
                 DISPLAY "Hey, we're in the capital."
              END-IF
              IF UNIVERSITYCITY
                 DISPLAY "Apply the rent surcharge!"
              END-IF
           END-IF
           SET CITYCODENOTVALID TO TRUE 
           DISPLAY CITYCODE
           GOBACK 
           .