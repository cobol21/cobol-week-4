       IDENTIFICATION DIVISION.
       PROGRAM-ID. CONTROL5.
       AUTHOR. NitroEz.4
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 VALIDATIONRETURNCODE  PIC 9     VALUE 9.
          88 DATEISOK                     VALUE 0.
          88 DATEISINVALID                VALUE 1 THRU 8.
          88 VALIDCODESUPPLIED            VALUE 0 THRU 8.

       01 DATEERRORMESSAGE      PIC X(35) VALUE SPACES.
          88 DATENOTNUMERIC               VALUE
                "Error - The date must be numeric".
          88 YEARISZERO                   VALUE
                "Error - The year cannot be zero".
          88 MONTHISZERO                  VALUE
                "Error - The month cannot be zero".
          88 DAYISZERO                    VALUE
                "Error - The day cannot be zero".
          88 YEARPASSED                   VALUE
                "Error - Year has already passed".
          88 MONTHTOOBIG                  VALUE
                "Error - Month is greater than 12".
          88 DAYTOOBIG                    VALUE
                "Error - Day greater than 31".
          88 TOOBIGFORMONTH               VALUE
                "Error - Day too big for this month".

       PROCEDURE DIVISION.
       BEGIN.
           PERFORM VALIDATEDATE UNTIL VALIDCODESUPPLIED
           EVALUATE VALIDATIONRETURNCODE
           WHEN 0
                SET DATEISOK TO TRUE
           WHEN 1
                SET DATENOTNUMERIC TO TRUE
           WHEN 2
                SET YEARISZERO TO TRUE
           WHEN 3
                SET MONTHISZERO TO TRUE
           WHEN 4
                SET DAYISZERO TO TRUE
           WHEN 5
                SET YEARPASSED TO TRUE
           WHEN 6
                SET MONTHTOOBIG TO TRUE
           WHEN 7
                SET DAYTOOBIG TO TRUE
           WHEN 8
                SET TOOBIGFORMONTH TO TRUE
           END-EVALUATE

           IF DATEISINVALID THEN
              DISPLAY DATEERRORMESSAGE
           END-IF

           IF DATEISOK
              DISPLAY "Date is Ok"
           END-IF
           GOBACK 
           .
              
       VALIDATEDATE.
           DISPLAY "Enter a validation return code (0-8) - "
              WITH NO ADVANCING
           ACCEPT VALIDATIONRETURNCODE.