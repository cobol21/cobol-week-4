       IDENTIFICATION DIVISION.
       PROGRAM-ID. CONTROL4.
       AUTHOR. NitroEz.
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT STUDENTFILE ASSIGN TO "GRADE.DAT"
           ORGANIZATION IS LINE SEQUENTIAL.
       DATA DIVISION.
       FILE SECTION.
       FD STUDENTFILE.
       01 STUDENTDETAILS.
          88 ENDOFSTUDENTFILE            VALUE HIGH-VALUE.
       05 STUDENTID            PIC X(8).
          05 STUDENTNAME       PIC X(16).
          05 COURSECODE        PIC X(8).
          05 GRADE             PIC X(2).

       PROCEDURE DIVISION.
       BEGIN.
           OPEN INPUT STUDENTFILE
           READ STUDENTFILE
           AT END
              SET ENDOFSTUDENTFILE TO TRUE
           END-READ

           PERFORM UNTIL ENDOFSTUDENTFILE
                   READ STUDENTFILE
                   AT END
                      SET ENDOFSTUDENTFILE TO TRUE
                   END-READ
                   IF NOT ENDOFSTUDENTFILE THEN
                 DISPLAY STUDENTNAME SPACE STUDENTID SPACE COURSECODE SP
      -               ACE GRADE
                   END-IF             
           END-PERFORM
           CLOSE STUDENTFILE
           .